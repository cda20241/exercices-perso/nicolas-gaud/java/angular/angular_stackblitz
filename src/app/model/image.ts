export interface Image {
  id: number;
  name: string;
  dateTaken: Date;
  isFront: boolean;
  statusCode: string;
  userId: number;
  imageTypeId: number;
}
