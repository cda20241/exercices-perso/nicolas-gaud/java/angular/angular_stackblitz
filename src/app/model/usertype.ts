export interface UserType {
  id: number;
  label: string;
  codeType: string;
}
