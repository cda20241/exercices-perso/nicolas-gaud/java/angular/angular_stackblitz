import {Image} from "./image";

export interface Marker {
  id: number;
  name: string;
  description: string;
  latitude: number;
  longitude: number;
  isUrbex: boolean;
  mapsLink: string;
  typeLabel: string;
  typeCode: string;
  statusLabel: string;
  statusCode: string;
  motifLabel: string;
  nickname: string;
  departmentName: string;
  departmentId: string;
  regionName: string;
  regionId: number;
  countryName: string;
  countryId: number;
  firstImage: string;
  dateVisited: Date;
  images: Image[];
}
