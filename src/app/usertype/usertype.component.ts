import {Component, OnInit} from '@angular/core';

import { UsertypeService } from '../service/usertype.service';
import {Observable} from "rxjs";
import {UserType} from "../model/usertype";


@Component({
  selector: 'app-usertype',
  templateUrl: './usertype.component.html',
  styleUrls: ['./usertype.component.scss']
})
export class UsertypeComponent implements OnInit{

  constructor(
    private usertypeService: UsertypeService,
  ) {}

  // usertypes = this.usertypeService.getUsertypes();

  usertypes!: Observable<UserType[]>;

  ngOnInit(): void {
    this.usertypes =  this.usertypeService.getUsertypes();
  }

}
