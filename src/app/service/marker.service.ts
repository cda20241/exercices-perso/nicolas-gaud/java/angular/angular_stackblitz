import { HttpClient } from '@angular/common/http';
import { Marker } from '../model/marker';
import { Injectable } from '@angular/core';
import {lastValueFrom} from "rxjs";

const USER_API_BASE_URL = "http://localhost:8082";

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  markers: Marker[] = [];

  constructor(
    private http: HttpClient
  ) {}

  getMarkers(): Promise<Marker[]>{
    return lastValueFrom(this.http.get<Marker[]>(USER_API_BASE_URL + "/markers/all/4")) as Promise<Marker[]> ;
  }
}
