import { HttpClient } from '@angular/common/http';
import { UserType } from '../model/usertype';
import { Injectable } from '@angular/core';

const USER_API_BASE_URL = "http://localhost:8082";

@Injectable({
  providedIn: 'root'
})
export class UsertypeService {

  usertypes: UserType[] = [];

  constructor(
    private http: HttpClient
  ) {}

  getUsertypes(){
    return this.http.get<UserType[]>(USER_API_BASE_URL + "/userTypes/all");
  }
}
