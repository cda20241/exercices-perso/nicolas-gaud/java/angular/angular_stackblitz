import {Component, OnInit} from '@angular/core';

import {MarkerService} from '../service/marker.service';
import {Observable} from "rxjs";
import {Marker} from "../model/marker";
import {Image} from "../model/image";

@Component({
  selector: 'app-marker',
  templateUrl: './marker.component.html',
  styleUrls: ['./marker.component.scss']
})
export class MarkerComponent {

  constructor(
    private markerService: MarkerService,
  ) {}

  markers?: Marker[];

  async ngOnInit(){
    this.markers = await this.markerService.getMarkers();
  }

}
